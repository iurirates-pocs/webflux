CREATE TABLE pessoa (
  cpf VARCHAR(11) NOT NULL,
  nome VARCHAR(50) NULL,
  sobrenome VARCHAR(150) NULL,
  email VARCHAR(255) NULL,
  ativo BIT(1) NULL,
  CONSTRAINT pk_pessoa PRIMARY KEY (cpf)
);