package com.iurirates.poc.webflux.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class Oauth2Configuration {

    public String oauth2TokenEndpoint() {
        return String.format("%s", "/oauth/token");
    }
}
