package com.iurirates.poc.webflux.v1.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Builder
@Table(name = "pessoa")
@AllArgsConstructor
@NoArgsConstructor
public class Pessoa {

    @Id
    @NotNull
    @Column(name = "cpf")
    @Size(max = 11)
    private String cpf;

    @Column(name = "nome")
    @NotNull
    @Size(max = 50)
    private String nome;

    @Column(name = "sobrenome")
    @Size(max = 150)
    private String sobrenome;

    @Column(name = "email")
    @Size(max = 255)
    private String email;

    @Column(name = "ativo")
    private boolean ativo;

}
