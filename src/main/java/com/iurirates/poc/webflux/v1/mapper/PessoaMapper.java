package com.iurirates.poc.webflux.v1.mapper;

import com.iurirates.poc.webflux.v1.model.Pessoa;
import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.webjars.NotFoundException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@UtilityClass
@Slf4j
public class PessoaMapper {

    public Mono<PessoaResponse> map(Optional<Pessoa> pessoa){
        if (pessoa.isPresent()){
            log.info("Pessoa mapeada com sucesso");
            return Mono.just(
                    PessoaResponse.builder()
                            .cpf(pessoa.get().getCpf())
                            .ativo(pessoa.get().isAtivo())
                            .email(pessoa.get().getEmail())
                            .nome(pessoa.get().getNome())
                            .sobrenome(pessoa.get().getSobrenome())
                            .build()
            );
        }
        log.error("Erro ao mapear a pessoa");
        return Mono.error(new NotFoundException("Pessoa não localizada"));
    }

    public Mono<List<PessoaResponse>> map(List<Pessoa> pessoas){
        log.info("Mapeando uma lista de pessoas");
        return Mono.just(pessoas.stream()
                .map(pessoa -> map(pessoa)).collect(toList()));
    }

    public PessoaResponse map(Pessoa pessoa){
        if (pessoa != null){
            log.info("Pessoa mapeada com sucesso");
            return PessoaResponse.builder()
                            .cpf(pessoa.getCpf())
                            .ativo(pessoa.isAtivo())
                            .email(pessoa.getEmail())
                            .nome(pessoa.getNome())
                            .sobrenome(pessoa.getSobrenome())
                            .build();
        }
        log.error("Erro ao mapear a pessoa");
        return null;
    }

    public Pessoa map(PessoaCadastroRequest pessoaCadastroRequest){
        if(pessoaCadastroRequest != null){
            log.info("Pessoa cadastro mapeada com sucesso");
            return Pessoa.builder()
                    .cpf(pessoaCadastroRequest.getCpf())
                    .nome(pessoaCadastroRequest.getNome())
                    .sobrenome(pessoaCadastroRequest.getSobrenome())
                    .email(pessoaCadastroRequest.getEmail())
                    .ativo(pessoaCadastroRequest.isAtivo())
                    .build();
        }
        log.error("Erro ao mapear a pessoa cadastro");
        return null;
    }

    public Pessoa map(PessoaAtualizacaoRequest pessoaAtualizacaoRequest){
        if(pessoaAtualizacaoRequest != null){
            log.info("Pessoa atualização mapeada com sucesso");
            return Pessoa.builder()
                    .cpf(pessoaAtualizacaoRequest.getCpf())
                    .nome(pessoaAtualizacaoRequest.getNome())
                    .sobrenome(pessoaAtualizacaoRequest.getSobrenome())
                    .email(pessoaAtualizacaoRequest.getEmail())
                    .ativo(pessoaAtualizacaoRequest.isAtivo())
                    .build();
        }
        log.error("Erro ao mapear a pessoa atualização");
        return null;
    }

}
