package com.iurirates.poc.webflux.v1.controller;

import com.iurirates.poc.webflux.v1.controller.api.PessoaControllerApi;
import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaBuscaRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import com.iurirates.poc.webflux.v1.service.PessoaService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
public class PessoaController implements PessoaControllerApi {

    @Autowired
    private PessoaService pessoaService;

    @Override
    public Mono<PessoaResponse> buscarDadosPessoa(PessoaBuscaRequest request) {
        return pessoaService.buscarDadosPessoa(request);
    }

    @Override
    public Mono<List<PessoaResponse>> buscarPessoas() {
        return pessoaService.buscarPessoas();
    }

    @Override
    public Mono<PessoaResponse> cadastrarPessoa(PessoaCadastroRequest request) {
        return pessoaService.cadastrarPessoa(request);
    }

    @Override
    public Mono<Void> removerPessoa(String cpf) {
        return pessoaService.removerPessoa(cpf);
    }

    @Override
    public Mono<Void> atualizarPessoa(PessoaAtualizacaoRequest request) {
        return pessoaService.atualizarPessoa(request);
    }


}
