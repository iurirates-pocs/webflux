package com.iurirates.poc.webflux.v1.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Schema(
        description = "Parâmetros utilizados para a realização da consulta de uma pessoa."
)
@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PessoaBuscaRequest {

    @Schema(description = "CPF da pessoa sem máscara",
            example = "00100100100",
            required = true,
            title = "CPF da pessoa sem máscara")
    @NotNull
    @Valid
    private String cpf;

}
