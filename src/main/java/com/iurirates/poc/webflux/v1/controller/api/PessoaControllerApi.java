package com.iurirates.poc.webflux.v1.controller.api;

import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaBuscaRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

import javax.validation.Valid;
import java.util.List;

@Tag(name = "Pessoa",
        description = "Endpoints relacionados ao tratamento da Pessoa")
@RequestMapping("v1/pessoa")
public interface PessoaControllerApi {

    @CrossOrigin(origins = "*")
    @Operation(summary = "Busca uma pessoa pelo cpf",
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "A pessoa foi localizada com sucesso."),
                    @ApiResponse(responseCode = "404",
                    description = "A pessoa não foi localizada com sucesso.",
                            content = @Content(mediaType = "application/json",
                                    examples = {@ExampleObject(name = "Exemplo de um retorno da aplicação já tratato quando não foi localizado nenhuma pessoa, exibindo apenas a mensagem de erro.",
                                            value = "{erro: Pessoa não localizada}",
                                            summary = "exemplo retorno para pessoa não encontrada", externalValue = "exemplo de retorno para pessoa não encontrada")}                            )),
                    @ApiResponse(responseCode = "500",
                    description = "Ocorreu um erro interno na aplicação.",
                            content = @Content(mediaType = "application/json",
                                    examples = {@ExampleObject(name = "Exemplo de um erro interno da aplicação já tratato exibindo apenas a mensagem de erro.",
                                            value = "{erro: mensagem de erro}",
                                            summary = "exemplo de erro interno", externalValue = "exemplo de erro interno")}))},
            method = "buscarPessoa",
            tags = {"Pessoa",})
    @GetMapping(value = {"/"},
            produces = { MediaType.APPLICATION_JSON_VALUE })
    Mono<PessoaResponse> buscarDadosPessoa(PessoaBuscaRequest request);

    @CrossOrigin(origins = "*")
    @Operation(summary = "Retorna uma lista de todas as pessoas cadastradas",
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "As pessoas foram localizadas com sucesso."),
                    @ApiResponse(responseCode = "404",
                            description = "Não existem pessoas cadastradas para listar.",
                            content = @Content(mediaType = "application/json",
                                    examples = {@ExampleObject(name = "Exemplo de um retorno da aplicação já tratato quando não foi localizado nenhuma pessoa, exibindo apenas a mensagem de erro.",
                                            value = "{erro: Não existem pessoas para listar}",
                                            summary = "exemplo retorno para pessoas não encontradas", externalValue = "exemplo de retorno para pessoas não encontradas")}                            )),
                    @ApiResponse(responseCode = "500",
                            description = "Ocorreu um erro interno na aplicação.",
                            content = @Content(mediaType = "application/json",
                                    examples = {@ExampleObject(name = "Exemplo de um erro interno da aplicação já tratato exibindo apenas a mensagem de erro.",
                                            value = "{erro: mensagem de erro}",
                                            summary = "exemplo de erro interno", externalValue = "exemplo de erro interno")}))},
            method = "buscarPessoas",
            tags = {"Pessoas",})
    @GetMapping(value = {"/buscar-pessoas"},
            produces = { MediaType.APPLICATION_JSON_VALUE })
    Mono<List<PessoaResponse>> buscarPessoas();

    @CrossOrigin(origins = "*")
    @Operation(summary = "Realiza o cadastro de uma nova pessoa",
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "Pessoa criada com sucesso."),
                    @ApiResponse(responseCode = "400",
                            description = "Requisição inválida."),
                    @ApiResponse(responseCode = "422",
                            description = "Entidade não processável."),
                    @ApiResponse(responseCode = "500",
                            description = "Ocorreu um erro interno na aplicação.",
                            content = @Content(mediaType = "application/json",
                                    examples = {@ExampleObject(name = "Exemplo de um erro interno da aplicação já tratato exibindo apenas a mensagem de erro.",
                                            value = "{erro: mensagem de erro}",
                                            summary = "exemplo de erro interno", externalValue = "exemplo de erro interno")}))},
            method = "cadastrarPessoa",
            tags = {"Pessoas",})
    @PutMapping(value = {"/"},
            produces = { MediaType.APPLICATION_JSON_VALUE },
            consumes = { MediaType.APPLICATION_JSON_VALUE })
    Mono<PessoaResponse> cadastrarPessoa(@Valid @RequestBody PessoaCadastroRequest request);

    @CrossOrigin(origins = "*")
    @Operation(summary = "Remove o cadastro de uma pessoa pelo cpf sem a máscara",
            responses = {
                    @ApiResponse(responseCode = "204",
                            description = "Pessoa removida com sucesso."),
                    @ApiResponse(responseCode = "500",
                            description = "Ocorreu um erro interno na aplicação.",
                            content = @Content(mediaType = "application/json",
                                    examples = {@ExampleObject(name = "Exemplo de um erro interno da aplicação já tratato exibindo apenas a mensagem de erro.",
                                            value = "{erro: mensagem de erro}",
                                            summary = "exemplo de erro interno", externalValue = "exemplo de erro interno")}))},
            method = "removerPessoa",
            tags = {"Pessoas",})
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = {"/{cpf}"})
    Mono<Void> removerPessoa(@PathVariable("cpf") String cpf);

    @CrossOrigin(origins = "*")
    @Operation(summary = "Atualiza o cadastro de uma nova pessoa",
            responses = {
                    @ApiResponse(responseCode = "200",
                            description = "Pessoa atualizada com sucesso."),
                    @ApiResponse(responseCode = "400",
                            description = "Requisição inválida."),
                    @ApiResponse(responseCode = "404",
                            description = "Pessoa não localizada para ser atualizada."),
                    @ApiResponse(responseCode = "422",
                            description = "Entidade não processável."),
                    @ApiResponse(responseCode = "500",
                            description = "Ocorreu um erro interno na aplicação.",
                            content = @Content(mediaType = "application/json",
                                    examples = {@ExampleObject(name = "Exemplo de um erro interno da aplicação já tratato exibindo apenas a mensagem de erro.",
                                            value = "{erro: mensagem de erro}",
                                            summary = "exemplo de erro interno", externalValue = "exemplo de erro interno")}))},
            method = "atualizarPessoa",
            tags = {"Pessoas",})
    @PostMapping(value = {"/"},
            produces = { MediaType.APPLICATION_JSON_VALUE },
            consumes = { MediaType.APPLICATION_JSON_VALUE })
    Mono<Void> atualizarPessoa(@Valid @RequestBody PessoaAtualizacaoRequest request);

}
