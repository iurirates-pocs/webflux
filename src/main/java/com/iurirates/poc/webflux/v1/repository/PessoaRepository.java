package com.iurirates.poc.webflux.v1.repository;

import com.iurirates.poc.webflux.v1.model.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, String> {
}
