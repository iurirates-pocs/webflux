package com.iurirates.poc.webflux.v1.request;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Schema(
        description = "Parâmetros utilizados para a realização da consulta de uma pessoa."
)
@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PessoaCadastroRequest {

    @Schema(description = "CPF da pessoa sem máscara",
            example = "00100100100",
            required = true,
            title = "CPF da pessoa sem máscara")
    @NotNull
    @Valid
    private String cpf;

    @Schema(description = "Nome da pessoa",
            example = "Joao",
            required = true,
            title = "Nome da pessoa")
    @Valid
    @JsonProperty("nome")
    @NotNull
    @Size(max = 50)
    private String nome;

    @Schema(description = "Sobrenome da pessoa",
            example = "Silva",
            title = "Sobrenomed da Pessoa")
    @Valid
    @JsonProperty("sobrenome")
    @Size(max = 150)
    private String sobrenome;

    @Schema(description = "Email da pessoa com máscara",
            example = "joaosilva@gmail.com",
            title = "Email da pessoa com máscara")
    @Valid
    @JsonProperty("email")
    @Size(max = 255)
    private String email;

    @Schema(description = "Flag que indica se a situação da pessoa é ativa ou não",
            example = "true",
            title = "Flag que indica se a situação da pessoa é ativa ou não")
    @Valid
    @JsonProperty("ativo")
    private boolean ativo;

}
