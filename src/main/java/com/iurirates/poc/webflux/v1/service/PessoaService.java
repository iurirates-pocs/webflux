package com.iurirates.poc.webflux.v1.service;

import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaBuscaRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import reactor.core.publisher.Mono;

import java.util.List;

public interface PessoaService {
    Mono<PessoaResponse> buscarDadosPessoa(PessoaBuscaRequest request);

    Mono<List<PessoaResponse>> buscarPessoas();

    Mono<PessoaResponse> cadastrarPessoa(PessoaCadastroRequest request);

    Mono<Void> removerPessoa(String cpf);

    Mono<Void> atualizarPessoa(PessoaAtualizacaoRequest request);
}
