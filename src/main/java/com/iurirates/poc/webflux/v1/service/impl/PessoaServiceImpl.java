package com.iurirates.poc.webflux.v1.service.impl;

import com.iurirates.poc.webflux.v1.mapper.PessoaMapper;
import com.iurirates.poc.webflux.v1.model.Pessoa;
import com.iurirates.poc.webflux.v1.repository.PessoaRepository;
import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaBuscaRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import com.iurirates.poc.webflux.v1.service.PessoaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PessoaServiceImpl implements PessoaService {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Override
    public Mono<PessoaResponse> buscarDadosPessoa(PessoaBuscaRequest request) {
        log.info("Buscando os dados da pessoa com o id " + request.getCpf());
        return PessoaMapper.map(pessoaRepository.findById(request.getCpf()));
    }

    @Override
    public Mono<List<PessoaResponse>> buscarPessoas() {
        log.info("Buscando todas as pessoas cadastradas na aplicação");
        return PessoaMapper.map(pessoaRepository.findAll());
    }

    @Override
    public Mono<PessoaResponse> cadastrarPessoa(PessoaCadastroRequest request) {
        log.info("Realizado o cadastro de uma nova pessoa");
        return PessoaMapper.map(Optional.of(pessoaRepository.save(PessoaMapper.map(request))));
    }

    @Override
    public Mono<Void> removerPessoa(String cpf) {
        log.info("Removendo a pessoa pelo cpf " + cpf);
        pessoaRepository.deleteById(cpf);
        return Mono.just("Pessoa removida com sucesso").then();
    }

    @Override
    public Mono<Void> atualizarPessoa(PessoaAtualizacaoRequest request) {
        log.info("Realizado a atualização do cadastro de uma pessoa");
        Optional<Pessoa> pessoa = pessoaRepository.findById(request.getCpf());
        if (pessoa.isPresent()){
            Pessoa pessoaPersistir = pessoa.get();
            Pessoa pessoaRequest = PessoaMapper.map(request);

            pessoaPersistir.setAtivo(pessoaRequest.isAtivo());
            pessoaPersistir.setEmail(pessoaRequest.getEmail());
            pessoaPersistir.setNome(pessoaRequest.getNome());
            pessoaPersistir.setSobrenome(pessoaRequest.getSobrenome());

            pessoaRepository.save(pessoaPersistir);

            log.info("Pessoa atualizada com sucesso");
            return Mono.just("Pessoa atualizada com sucesso").then();
        }
        log.error("Erro ao atualizar a pessoa");
        return Mono.just("Não foi possível atualizar a pessoa").then();
    }
}
