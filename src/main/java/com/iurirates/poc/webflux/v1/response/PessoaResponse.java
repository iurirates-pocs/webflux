package com.iurirates.poc.webflux.v1.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Schema(
        description = "Response da Consulta de Pessoa"
)
@Validated
@JsonInclude(JsonInclude.Include.NON_NULL)
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PessoaResponse {

    @Schema(description = "CPF da pessoa sem máscara",
            example = "00100100100",
            required = true,
            title = "Cpf da pessoa")
    @Valid
    @JsonProperty("cpf")
    @NotNull
    @Size(max = 11)
    private String cpf;

    @Schema(description = "Nome da pessoa",
            example = "Joao",
            required = true,
            title = "Nome da pessoa")
    @Valid
    @JsonProperty("nome")
    @NotNull
    @Size(max = 50)
    private String nome;

    @Schema(description = "Sobrenome da pessoa",
            example = "Silva",
            title = "Sobrenomed da Pessoa")
    @Valid
    @JsonProperty("sobrenome")
    @Size(max = 150)
    private String sobrenome;

    @Schema(description = "Email da pessoa com máscara",
            example = "joaosilva@gmail.com",
            title = "Email da pessoa com máscara")
    @Valid
    @JsonProperty("email")
    @Size(max = 255)
    private String email;

    @Schema(description = "Flag que indica se a situação da pessoa é ativa ou não",
            example = "true",
            title = "Flag que indica se a situação da pessoa é ativa ou não")
    @Valid
    @JsonProperty("ativo")
    private boolean ativo;

}
