package com.iurirates.poc.webflux.v1.service;

import com.iurirates.poc.webflux.v1.mapper.PessoaMapper;
import com.iurirates.poc.webflux.v1.model.Pessoa;
import com.iurirates.poc.webflux.v1.repository.PessoaRepository;
import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaBuscaRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import com.iurirates.poc.webflux.v1.service.impl.PessoaServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class PessoaServiceImplTest {

    @InjectMocks
    private PessoaServiceImpl pessoaServiceImpl;

    @Mock
    private PessoaRepository pessoaRepository;

    @Rule
    public final ErrorCollector collector = new ErrorCollector();


    @Before
    public void init(){
        pessoaRepository.save(criarPessoa());
    }

    @After
    public void destroy(){
        pessoaServiceImpl.removerPessoa(criarPessoa().getCpf());
    }

    @Test
    public void buscarDadosPessoaTest(){
        //Arrange
        PessoaBuscaRequest pessoaBuscaRequest = criarPessoaBuscaRequest();
        Mono<PessoaResponse> pessoaResponseMono = criarMonoPessoaResponse(pessoaBuscaRequest);
        when(pessoaRepository.findById(pessoaBuscaRequest.getCpf())).thenReturn(Optional.of(criarPessoa()));

        //Act
        Mono<PessoaResponse> response = pessoaServiceImpl.buscarDadosPessoa(pessoaBuscaRequest);

        //Assert
        collector.checkThat(response.block().getCpf(), equalTo(pessoaResponseMono.block().getCpf()));
        collector.checkThat(response.block().isAtivo(), equalTo(pessoaResponseMono.block().isAtivo()));
        collector.checkThat(response.block().getEmail(), equalTo(pessoaResponseMono.block().getEmail()));
        collector.checkThat(response.block().getNome(), equalTo(pessoaResponseMono.block().getNome()));
        collector.checkThat(response.block().getSobrenome(), equalTo(pessoaResponseMono.block().getSobrenome()));
    }

    @Test
    public void buscarPessoasTest(){
        //Arrange
        List<Pessoa> pessoaList = criarListPessoa();
        when(pessoaRepository.findAll()).thenReturn(pessoaList);

        //Act
        Mono<List<PessoaResponse>> listaRetorno = pessoaServiceImpl.buscarPessoas();

        //Assert
        collector.checkThat(listaRetorno.block().isEmpty(), equalTo(false));
        collector.checkThat(listaRetorno.block().get(0).getCpf(), equalTo(pessoaList.get(0).getCpf()));
        collector.checkThat(listaRetorno.block().get(0).isAtivo(), equalTo(pessoaList.get(0).isAtivo()));
        collector.checkThat(listaRetorno.block().get(0).getEmail(), equalTo(pessoaList.get(0).getEmail()));
        collector.checkThat(listaRetorno.block().get(0).getNome(), equalTo(pessoaList.get(0).getNome()));
        collector.checkThat(listaRetorno.block().get(0).getSobrenome(), equalTo(pessoaList.get(0).getSobrenome()));
    }

    @Test
    public void cadastrarPessoaTest(){
        //Arrange
        PessoaCadastroRequest pessoaCadastroRequest = criarPessoaCadastroRequest();
        Pessoa pessoa = criarPessoa();
        when(pessoaRepository.save(PessoaMapper.map(pessoaCadastroRequest))).thenReturn(pessoa);

        //Act
        Mono<PessoaResponse> response = pessoaServiceImpl.cadastrarPessoa(pessoaCadastroRequest);

        //Assert
        collector.checkThat(response.block().getCpf(), equalTo(pessoa.getCpf()));
        collector.checkThat(response.block().isAtivo(), equalTo(pessoa.isAtivo()));
        collector.checkThat(response.block().getEmail(), equalTo(pessoa.getEmail()));
        collector.checkThat(response.block().getNome(), equalTo(pessoa.getNome()));
        collector.checkThat(response.block().getSobrenome(), equalTo(pessoa.getSobrenome()));
    }

    @Test
    public void removerPessoaTest(){
        //Arrange
        String cpf = "00100100100";

        //Act
        pessoaServiceImpl.removerPessoa(cpf);

        //Assert
        verify(pessoaRepository, times(1)).deleteById(cpf);
    }

    @Test
    public void atualizarPessoaTest(){
        //Arrange
        PessoaAtualizacaoRequest pessoaAtualizacaoRequest = criarPessoaAtualizacaoRequest();
        Pessoa pessoa = criarPessoa();

        //Act
        pessoaServiceImpl.atualizarPessoa(pessoaAtualizacaoRequest);

        //Assert
        verify(pessoaRepository, times(1)).save(pessoa);
    }

    private PessoaBuscaRequest criarPessoaBuscaRequest(){
        return PessoaBuscaRequest.builder()
                .cpf("00100100100")
                .build();
    }

    private Mono<PessoaResponse> criarMonoPessoaResponse(PessoaBuscaRequest pessoaBuscaRequest){
        return Mono.just(
                PessoaResponse.builder()
                        .cpf(pessoaBuscaRequest.getCpf())
                        .ativo(true)
                        .email("teste@teste.com.br")
                        .nome("Joao")
                        .sobrenome("Teste")
                        .build()
        );
    }

    private List<Pessoa> criarListPessoa(){
        ArrayList<Pessoa> listaPessoas = new ArrayList<>();
        listaPessoas.add(Pessoa.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build());
        return listaPessoas;
    }

    private PessoaCadastroRequest criarPessoaCadastroRequest(){
        return PessoaCadastroRequest.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build();
    }

    private PessoaAtualizacaoRequest criarPessoaAtualizacaoRequest(){
        return PessoaAtualizacaoRequest.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("testeatualizaco@teste.com.br")
                .nome("Joaos")
                .sobrenome("Teste Atualizado")
                .build();
    }

    private Pessoa criarPessoa(){
        return Pessoa.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build();
    }
}
