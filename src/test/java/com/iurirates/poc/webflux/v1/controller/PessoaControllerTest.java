package com.iurirates.poc.webflux.v1.controller;

import com.iurirates.poc.webflux.v1.mapper.PessoaMapper;
import com.iurirates.poc.webflux.v1.model.Pessoa;
import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaBuscaRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import com.iurirates.poc.webflux.v1.service.PessoaService;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class PessoaControllerTest {

    @InjectMocks
    private PessoaController pessoaController;

    @Mock
    private PessoaService pessoaService;

    @Rule
    public final ErrorCollector collector = new ErrorCollector();

    @Before
    public void init(){
        pessoaService.cadastrarPessoa(criarPessoaCadastroRequest());
    }

    @After
    public void destroy(){
        pessoaService.removerPessoa(criarPessoaCadastroRequest().getCpf());
    }

    @Test
    public void buscarDadosPessoaTest(){
        //Arrange
        PessoaBuscaRequest pessoaBuscaRequest = criarPessoaBuscaRequest();
        Mono<PessoaResponse> pessoaResponseMono = criarMonoPessoaResponse(pessoaBuscaRequest);
        when(pessoaService.buscarDadosPessoa(pessoaBuscaRequest)).thenReturn(pessoaResponseMono);

        //Act
        Mono<PessoaResponse> response = pessoaController.buscarDadosPessoa(pessoaBuscaRequest);

        //Assert
        collector.checkThat(response.block().getCpf(), equalTo(pessoaResponseMono.block().getCpf()));
        collector.checkThat(response.block().isAtivo(), equalTo(pessoaResponseMono.block().isAtivo()));
        collector.checkThat(response.block().getEmail(), equalTo(pessoaResponseMono.block().getEmail()));
        collector.checkThat(response.block().getNome(), equalTo(pessoaResponseMono.block().getNome()));
        collector.checkThat(response.block().getSobrenome(), equalTo(pessoaResponseMono.block().getSobrenome()));
    }

    @Test
    public void buscarPessoasTest(){
        //Arrange
        Mono<List<PessoaResponse>> pessoaListResponseMono = criarMonoListPessoaResponse();
        when(pessoaService.buscarPessoas()).thenReturn(pessoaListResponseMono);

        //Act
        Mono<List<PessoaResponse>> listaRetorno = pessoaController.buscarPessoas();

        //Assert
        collector.checkThat(listaRetorno.block().isEmpty(), equalTo(false));
        collector.checkThat(listaRetorno.block().get(0).getCpf(), equalTo(pessoaListResponseMono.block().get(0).getCpf()));
        collector.checkThat(listaRetorno.block().get(0).isAtivo(), equalTo(pessoaListResponseMono.block().get(0).isAtivo()));
        collector.checkThat(listaRetorno.block().get(0).getEmail(), equalTo(pessoaListResponseMono.block().get(0).getEmail()));
        collector.checkThat(listaRetorno.block().get(0).getNome(), equalTo(pessoaListResponseMono.block().get(0).getNome()));
        collector.checkThat(listaRetorno.block().get(0).getSobrenome(), equalTo(pessoaListResponseMono.block().get(0).getSobrenome()));
    }

    @Test
    public void cadastrarPessoaTest(){
        //Arrange
        PessoaCadastroRequest pessoaCadastroRequest = criarPessoaCadastroRequest();
        Mono<PessoaResponse> pessoaResponseMono = criarMonoPessoaResponseCadastro(pessoaCadastroRequest);
        when(pessoaService.cadastrarPessoa(pessoaCadastroRequest)).thenReturn(pessoaResponseMono);

        //Act
        Mono<PessoaResponse> response = pessoaController.cadastrarPessoa(pessoaCadastroRequest);

        //Assert
        collector.checkThat(response.block().getCpf(), equalTo(pessoaResponseMono.block().getCpf()));
        collector.checkThat(response.block().isAtivo(), equalTo(pessoaResponseMono.block().isAtivo()));
        collector.checkThat(response.block().getEmail(), equalTo(pessoaResponseMono.block().getEmail()));
        collector.checkThat(response.block().getNome(), equalTo(pessoaResponseMono.block().getNome()));
        collector.checkThat(response.block().getSobrenome(), equalTo(pessoaResponseMono.block().getSobrenome()));
    }

    @Test
    public void removerPessoaTest(){
        //Arrange
        String cpf = "00100100100";

        //Act
        pessoaController.removerPessoa(cpf);

        //Assert
        verify(pessoaService, times(1)).removerPessoa(cpf);
    }

    @Test
    public void atualizarPessoaTest(){
        //Arrange
        PessoaAtualizacaoRequest pessoaAtualizacaoRequest = criarPessoaAtualizacaoRequest();

        //Act
        pessoaController.atualizarPessoa(pessoaAtualizacaoRequest);

        //Assert
        verify(pessoaService, times(1)).atualizarPessoa(pessoaAtualizacaoRequest);
    }

    private PessoaBuscaRequest criarPessoaBuscaRequest(){
        return PessoaBuscaRequest.builder()
                .cpf("00100100100")
                .build();
    }

    private Mono<PessoaResponse> criarMonoPessoaResponse(PessoaBuscaRequest pessoaBuscaRequest){
        return Mono.just(
                PessoaResponse.builder()
                        .cpf(pessoaBuscaRequest.getCpf())
                        .ativo(true)
                        .email("teste@teste.com.br")
                        .nome("Joao")
                        .sobrenome("Teste")
                        .build()
        );
    }

    private Mono<PessoaResponse> criarMonoPessoaResponseCadastro(PessoaCadastroRequest pessoaCadastroRequest){
        return Mono.just(
                PessoaResponse.builder()
                        .cpf(pessoaCadastroRequest.getCpf())
                        .ativo(pessoaCadastroRequest.isAtivo())
                        .email(pessoaCadastroRequest.getEmail())
                        .nome(pessoaCadastroRequest.getNome())
                        .sobrenome(pessoaCadastroRequest.getSobrenome())
                        .build()
        );
    }

    private Mono<List<PessoaResponse>> criarMonoListPessoaResponse(){
        List<Pessoa> pessoas = criarListPessoa();
        return Mono.just(pessoas.stream()
                .map(pessoa -> PessoaMapper.map(pessoa)).collect(toList()));
    }

    private List<Pessoa> criarListPessoa(){
        ArrayList<Pessoa> listaPessoas = new ArrayList<>();
        listaPessoas.add(Pessoa.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build());
        return listaPessoas;
    }

    private PessoaCadastroRequest criarPessoaCadastroRequest(){
        return PessoaCadastroRequest.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build();
    }

    private PessoaAtualizacaoRequest criarPessoaAtualizacaoRequest(){
        return PessoaAtualizacaoRequest.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("testeatualizaco@teste.com.br")
                .nome("Joaos")
                .sobrenome("Teste Atualizado")
                .build();
    }
}
