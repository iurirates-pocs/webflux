package com.iurirates.poc.webflux.v1.mapper;

import com.iurirates.poc.webflux.v1.model.Pessoa;
import com.iurirates.poc.webflux.v1.request.PessoaAtualizacaoRequest;
import com.iurirates.poc.webflux.v1.request.PessoaBuscaRequest;
import com.iurirates.poc.webflux.v1.request.PessoaCadastroRequest;
import com.iurirates.poc.webflux.v1.response.PessoaResponse;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;
import org.webjars.NotFoundException;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(SpringRunner.class)
public class PessoaMapperTest {

    @Rule
    public final ErrorCollector collector = new ErrorCollector();

    @Test
    public void monoPessoaResponseMapTest(){
        //Arrange
        Optional<Pessoa> pessoa = criarOptionalPessoa();
        Mono<PessoaResponse> responseValidacao = criarMonoPessoaResponse();

        //Act
        Mono<PessoaResponse> response = PessoaMapper.map(pessoa);

        //Assert
        collector.checkThat(response.block().getCpf(), equalTo(responseValidacao.block().getCpf()));
        collector.checkThat(response.block().isAtivo(), equalTo(responseValidacao.block().isAtivo()));
        collector.checkThat(response.block().getEmail(), equalTo(responseValidacao.block().getEmail()));
        collector.checkThat(response.block().getNome(), equalTo(responseValidacao.block().getNome()));
        collector.checkThat(response.block().getSobrenome(), equalTo(responseValidacao.block().getSobrenome()));
    }

    @Test(expected = NotFoundException.class)
    public void monoPessoaResponseMapOptionalEmptyTest(){
        //Arrange
        Optional<Pessoa> pessoa = Optional.empty();
        Mono<PessoaResponse> responseValidacao = criarMonoPessoaResponseVazio();

        //Act
        Mono<PessoaResponse> response = PessoaMapper.map(pessoa);

        //Assert
        collector.checkThat(response.block(), equalTo("Pessoa não localizada"));
    }

    @Test
    public void monoListPessoaResponseMapTest(){
        //Arrange
        List<Pessoa> listaPessoas = criarListPessoa();
        Mono<List<PessoaResponse>> responseValidacao = criarMonoListPessoaResponse();

        //Act
        Mono<List<PessoaResponse>> response = PessoaMapper.map(listaPessoas);

        //Assert
        collector.checkThat(response.block().size(), equalTo(1));
        collector.checkThat(response.block().get(0).getCpf(), equalTo(responseValidacao.block().get(0).getCpf()));
        collector.checkThat(response.block().get(0).isAtivo(), equalTo(responseValidacao.block().get(0).isAtivo()));
        collector.checkThat(response.block().get(0).getEmail(), equalTo(responseValidacao.block().get(0).getEmail()));
        collector.checkThat(response.block().get(0).getNome(), equalTo(responseValidacao.block().get(0).getNome()));
        collector.checkThat(response.block().get(0).getSobrenome(), equalTo(responseValidacao.block().get(0).getSobrenome()));
    }

    @Test
    public void pessoaResponseMapTest(){
        //Arrange
        Pessoa pessoa = criarPessoa();
        PessoaResponse responseValidacao = criarPessoaResponse();

        //Act
        PessoaResponse response = PessoaMapper.map(pessoa);

        //Assert
        collector.checkThat(response.getCpf(), equalTo(responseValidacao.getCpf()));
        collector.checkThat(response.isAtivo(), equalTo(responseValidacao.isAtivo()));
        collector.checkThat(response.getEmail(), equalTo(responseValidacao.getEmail()));
        collector.checkThat(response.getNome(), equalTo(responseValidacao.getNome()));
        collector.checkThat(response.getSobrenome(), equalTo(responseValidacao.getSobrenome()));
    }

    @Test
    public void nullPessoaResponseMapTest(){
        //Act
        PessoaResponse response = PessoaMapper.map((Pessoa) null);

        //Assert
        collector.checkThat(response, equalTo(null));
    }

    @Test
    public void pessoaCadastroRequestMapTest(){
        //Arrange
        PessoaCadastroRequest pessoaCadastroRequest = criarPessoaCadastroRequest();
        Pessoa responseValidacao = criarPessoa();

        //Act
        Pessoa response = PessoaMapper.map(pessoaCadastroRequest);

        //Assert
        collector.checkThat(response.getCpf(), equalTo(responseValidacao.getCpf()));
        collector.checkThat(response.isAtivo(), equalTo(responseValidacao.isAtivo()));
        collector.checkThat(response.getEmail(), equalTo(responseValidacao.getEmail()));
        collector.checkThat(response.getNome(), equalTo(responseValidacao.getNome()));
        collector.checkThat(response.getSobrenome(), equalTo(responseValidacao.getSobrenome()));
    }

    @Test
    public void nullPessoaCadastrpRequestMapTest(){
        //Act
        Pessoa response = PessoaMapper.map((PessoaCadastroRequest) null);

        //Assert
        collector.checkThat(response, equalTo(null));
    }

    @Test
    public void pessoaAtualizacaoMapTest(){
        //Arrange
        PessoaAtualizacaoRequest pessoaAtualizacaoRequest = criarPessoaAtualizacaoRequest();
        Pessoa responseValidacao = criarPessoaAtualizacao();

        //Act
        Pessoa response = PessoaMapper.map(pessoaAtualizacaoRequest);

        //Assert
        collector.checkThat(response.getCpf(), equalTo(responseValidacao.getCpf()));
        collector.checkThat(response.isAtivo(), equalTo(responseValidacao.isAtivo()));
        collector.checkThat(response.getEmail(), equalTo(responseValidacao.getEmail()));
        collector.checkThat(response.getNome(), equalTo(responseValidacao.getNome()));
        collector.checkThat(response.getSobrenome(), equalTo(responseValidacao.getSobrenome()));
    }

    @Test
    public void nullPessoaAtualizacaoMapTest(){
        //Act
        Pessoa response = PessoaMapper.map((PessoaAtualizacaoRequest) null);

        //Assert
        collector.checkThat(response, equalTo(null));
    }

    private Optional<Pessoa> criarOptionalPessoa(){
        return Optional.of(Pessoa.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build());
    }

    private Mono<PessoaResponse> criarMonoPessoaResponse(){
        return Mono.just(
                PessoaResponse.builder()
                        .cpf("00100100100")
                        .ativo(true)
                        .email("teste@teste.com.br")
                        .nome("Joao")
                        .sobrenome("Teste")
                        .build()
        );
    }

    private PessoaResponse criarPessoaResponse(){
        return PessoaResponse.builder()
                        .cpf("00100100100")
                        .ativo(true)
                        .email("teste@teste.com.br")
                        .nome("Joao")
                        .sobrenome("Teste")
                        .build();
    }

    private Mono<PessoaResponse> criarMonoPessoaResponseVazio(){
        return Mono.error(new NotFoundException("Pessoa não localizada"));
    }

    private List<Pessoa> criarListPessoa(){
        ArrayList<Pessoa> listaPessoas = new ArrayList<>();
        listaPessoas.add(Pessoa.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build());
        return listaPessoas;
    }

    private Mono<List<PessoaResponse>> criarMonoListPessoaResponse(){
        List<Pessoa> pessoas = criarListPessoa();
        return Mono.just(pessoas.stream()
                .map(pessoa -> PessoaMapper.map(pessoa)).collect(toList()));
    }

    private Pessoa criarPessoa(){
        return Pessoa.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build();
    }

    private Pessoa criarPessoaAtualizacao(){
        return Pessoa.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("testeatualizaco@teste.com.br")
                .nome("Joaos")
                .sobrenome("Teste Atualizado")
                .build();
    }

    private PessoaCadastroRequest criarPessoaCadastroRequest(){
        return PessoaCadastroRequest.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("teste@teste.com.br")
                .nome("Joao")
                .sobrenome("Teste")
                .build();
    }

    private PessoaAtualizacaoRequest criarPessoaAtualizacaoRequest(){
        return PessoaAtualizacaoRequest.builder()
                .cpf("00100100100")
                .ativo(true)
                .email("testeatualizaco@teste.com.br")
                .nome("Joaos")
                .sobrenome("Teste Atualizado")
                .build();
    }
}
