FROM amazoncorretto:11-alpine-jdk
ADD target/webflux-1.0.0-SNAPSHOT.jar webflux-1.0.0.jar
ENTRYPOINT ["java","-jar","/webflux-1.0.0.jar"]