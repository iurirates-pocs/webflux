## Poc Webflux
Projeto contendo uma POC utilizando Webflux para implementação de um cadastro de pessoas com todas as funcionalidades de um CRUD.

Tecnologias utilizadas na implementação
- Spring Boot 2.5.6
- Java 11
- Lombok


## Persistência de Dados
Para a persistência de dados está sendo utilizado JPA com um banco de dados H2 em memória

Visando garantir que a aplicação possua já uma carga inicial do banco de dados com a estrutura necessária e também pensando em versionamento do mesmo está sendo utilizado o Flyway.

## Testes Unitários
Foram implementados testes unitários utilizando Junit e RestAssured

## Containerização
No arquivo Dockerfile está a configuração para que o build da aplicação gere uma máquina docker

## CI/CD
Foi implementado o padrão do Git para CI/CD onde é rodados os testes unitários e no final da pipeline gera uma imagem

## Build da Aplicação
Para realizar o build da aplicação foi utilizado o Maven como repositório de dependências

Visando facilitar o Build recomendo instalar o Intellij pois o mesmo já vem com o Maven pré-configurado

Deve-se baixar o docker em https://www.docker.com/products/docker-desktop e instalar o mesmo

Dentro do Intellij após ter importado o projeto deve-se procurar a guia maven (geralmente na direita) e rodar o comando:
> mvn clean package

Após finalizar o build da aplicação pelo maven no diretório raiz do projeto deve-se rodar o comando:
> docker build --tag=webflux:latest .

Após finalizar o download dos pacotes e a execução do comando ocorrer com sucesso deve-se executar o comando:
> docker run -p8080:8080 weblflux:latest

Assim que finalizar o build da aplicação para acessar a mesma deve-se acessar o endereço abaixo no seu navegador:
> http://localhost:8080/swagger-ui.html 